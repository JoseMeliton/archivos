package com.MiHolaMundo.cotizasdor.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet {
	
private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServerException, IOException{
		
		resp.setContentType("text/html");
		
		PrintWriter output = resp.getWriter();
		output.println("HTML");
		output.println("<body>");
		output.println("<h3>HELLO !! FROM MY SERVLET !! </h3>");
		output.println("<hr>");
		output.println("</HTML>");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServerException, IOException{
	
		
		doGet(req,resp);
	}

}
